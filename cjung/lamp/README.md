# Ansible Collection - cjung.lamp

This is a simple example collection to demonstrate how to use Ansible Collections. Therefore the role is not supposed to be used in any production scenario and only provided for learning purposes.

Read the full [Documentation](https://www.jung-christian.de).
